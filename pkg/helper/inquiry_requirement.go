package helper

import (
	"fmt"
	"strconv"

	"bitbucket.org/udevs/example_api_gateway/api/models"
	"bitbucket.org/udevs/example_api_gateway/genproto/inquiry_service"
)

func ParseRequirementArrays(values []models.InquiryRequirement) []models.InquiryRequirementArrays {
	requirementArrays := []models.InquiryRequirementArrays{}

	for _, attributes := range values {
		var have bool
		for i, v := range requirementArrays {
			if v.ID == attributes.RequirementAttribute.ID {
				have = true
				requirementArrays[i].RequirementValues = append(requirementArrays[i].RequirementValues,
					models.RequirementValue{
						Id:                     attributes.RequirementValue.Id,
						RequirementAttributeId: attributes.RequirementAttribute.ID,
						Value:                  attributes.RequirementValue.Value,
					},
				)
				break
			}
		}

		if !have {
			requirementArrays = append(requirementArrays, models.InquiryRequirementArrays{
				ID:        attributes.RequirementAttribute.ID,
				Name:      attributes.RequirementAttribute.Name,
				DataStype: attributes.RequirementAttribute.DataType,
				InputType: attributes.RequirementAttribute.InputType,
				RequirementValues: []models.RequirementValue{
					{
						Id:                     attributes.RequirementValue.Id,
						RequirementAttributeId: attributes.RequirementAttribute.ID,
						Value:                  attributes.RequirementValue.Value,
					},
				},
			})
		}
	}

	return requirementArrays
}

func GenerateInquiryRequirement(requirementArrays []models.InquiryRequirementArrays) []*inquiry_service.InquiryRequirement {
	var requirementValues []*inquiry_service.InquiryRequirement

	for _, requirement := range requirementArrays {
		for _, value := range requirement.RequirementValues {
			requirementValues = append(requirementValues, &inquiry_service.InquiryRequirement{
				RequirementAttribute: &inquiry_service.Attribute{
					Id:        requirement.ID,
					Name:      requirement.Name,
					InputType: requirement.InputType,
					DataType:  requirement.DataStype,
				},
				RequirementValue: &inquiry_service.RequirementValue{
					Id:                     value.Id,
					RequirementAttributeId: requirement.ID,
					Value:                  value.Value,
				},
			})
		}
	}

	fmt.Printf("\n\n%+v\n\n", requirementValues)

	return requirementValues
}

func GenerateTimeTables(time_tables []models.InquiryTimeTable) []*inquiry_service.InquiryTimeTable {
	response := make([]*inquiry_service.InquiryTimeTable, 0)

	for _, time_table := range time_tables {
		response = append(response,
			&inquiry_service.InquiryTimeTable{
				Dates:                time_table.Dates,
				StartTime:            time_table.StartTime,
				EndTime:              time_table.EndTime,
				NumberOfPartisipants: time_table.NumberOfPartisipants,
				NumberOfApplicants:   time_table.NumberOfApplicants,
			},
		)
	}

	return response
}

func GetPartisipantRolePosition(key string) int64 {
	switch key {
	case "professional":
		return 2
	case "consultant":
		return 3
	default:
		return 1
	}
}

func GetVariable(key string, variables *inquiry_service.GetAllVariablesResponse) int64 {
	for _, v := range variables.GlobalVariables {
		if v.Key == key {
			n, err := strconv.Atoi(v.Value)
			if err != nil {
				return 0
			}

			return int64(n)
		}
	}

	return 0
}
